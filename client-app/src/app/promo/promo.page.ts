import { Component, ChangeDetectorRef } from "@angular/core";
import { Platform } from "@ionic/angular";
import { BeaconInteractive } from "../beacon/beacon-interactive";

@Component({
  selector: "page-promo",
  templateUrl: "promo.page.html"
})
export class PromoPage {
  constructor(
    private platform: Platform,
    private change: ChangeDetectorRef,
    private beacon: BeaconInteractive
  ) {}
}
