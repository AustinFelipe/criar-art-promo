import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  { path: "", redirectTo: "promo", pathMatch: "full" },
  {
    path: "addpromo",
    loadChildren: () =>
      import("./addpromo/addpromo.module").then(m => m.AddPromoPageModule)
  },
  {
    path: "login",
    loadChildren: () =>
      import("./login/login.module").then(m => m.LoginPageModule)
  },
  {
    path: "promo",
    loadChildren: () =>
      import("./promo/promo.module").then(m => m.PromoPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
