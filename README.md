# Criar Art Promo App

Este projeto é referente a entrega de atividade da Fiap.

# Estrutura de pastas

- `/client-app` é o app utilizado para gerenciar promoções (perfil admin) e verificar novas promoções (perfil customer)
- `/server` é o backend do app

# Tecnologias

- Ionic Framework
- iBeacon
- Firedb
- Angular (ionic)

# Como é utilizado Beacon?

A tecnologia iBeacon é utilizada para disparar promoções criadas por lojas para clientes que estão próximas da origem Beacon.

# Telas da aplicação

- Login
- Gerenciamento promoções (visão loja/admin)
- Visualização de promoções (visão customer)

# Como rodar o projeto `clientapp`

- Development

`ionic serve -c`

- Criar pacote para publicar na Google Play

`ionic cordova build android --prod --release`

- Criar pacote para publicar na App Store

`ionic cordova build ios --prod`
